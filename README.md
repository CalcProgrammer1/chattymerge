# chattymerge

Merge SMS history from two chatty-history.db databases

Rename the files `source.db` and `destination.db` and then run chattymerge.py in the directory containing them.

SMS history, including new users and threads, will be copied from source into destination.  Threads will be merged and duplicates skipped.
