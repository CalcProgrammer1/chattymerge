########################################################################
# chattymerge.py
#
# Merges Chatty message history from a source database into
# a destination database.  Run multiple operating systems on
# your PinePhone?  Use this script to combine your Chatty
# databases and you won't lose your SMS history.
#
# Chatty database is located at ~/.purple/chatty/db/chatty-history.db
#
# Script by Adam Honse <calcprogrammer1@gmail.com>
########################################################################

import sqlite3
from sqlite3 import Error

def create_connection(db_file):
	conn = None
	try:
		conn = sqlite3.connect(db_file)
	except Error as e:
		print(e)

	return conn

def merge_users(cur_d, cur_s):
	# Create a list of in-use IDs in the destination database
	in_use_ids = []

	for row in cur_d.execute("SELECT id FROM users ORDER BY id"):
		this_id = row[0]
		if this_id not in in_use_ids:
			in_use_ids.append(this_id)

	print("In-use IDs: " + str(in_use_ids))

	# Loop through users table on both databases
	# Add entries to destination database with new id
	for row_s in cur_s.execute("SELECT * FROM users ORDER BY id"):
		match = 0
		for row_d in cur_d.execute("SELECT * FROM users ORDER BY id"):
			if(row_s[0] == row_d[0]):
				if(row_s[1] == row_d[1]):
					print("ID " + str(row_s[0]) + " matches")
					print("Username " + row_s[1] + " matches")
					match = 1
					break
				else:
					print("Username conflict at ID " + str(row_s[0]))
					print("Source: " + row_s[1] + ", Destination: " + row_d[1])
					
					# Search destination table to see if this username exists
					# at a different ID.  If it does, don't add a new entry
					for row_d in cur_d.execute("SELECT * FROM users ORDER BY id"):
						if(row_s[1] == row_d[1]):
							print("Username " + row_s[1] + " matched at destination ID " + str(row_d[0]))
							match = 1
							break
					
					if(match == 0):
						new_id = row_s[0] + 1
						while new_id in in_use_ids:
							new_id = new_id + 1

						row_s_list = []
						row_s_list.extend(row_s)
						row_s_list[0] = new_id
						row_s_list[3] = ''
						row_s = tuple(row_s_list)
						print(row_s)
						cur_d.execute("INSERT INTO users VALUES " + str(row_s))
						in_use_ids.append(new_id)
						match = 1
				break

		if(match == 0):
			print("ID " + str(row_s[0]) + " does not match")

			# Search destination table to see if this username exists
			# at a different ID.  If it does, don't add a new entry
			for row_d in cur_d.execute("SELECT * FROM users ORDER BY id"):
				if(row_s[1] == row_d[1]):
					print("Username " + row_s[1] + " matched at destination ID " + str(row_d[0]))
					match = 1
					break

			if(match == 0):
				if row_s[0] not in in_use_ids:
					row_s_list = []
					row_s_list.extend(row_s)
					row_s_list[3] = ''
					row_s = tuple(row_s_list)
					print(row_s)
					cur_d.execute("INSERT INTO users VALUES " + str(row_s))

def merge_threads(cur_d, cur_s):
	# Create a list of in-use IDs in the destination database
	in_use_ids = []
	in_use_ids_members = []

	for row in cur_d.execute("SELECT id FROM threads ORDER BY id"):
		this_id = row[0]
		if this_id not in in_use_ids:
			in_use_ids.append(this_id)

	for row in cur_d.execute("SELECT id FROM thread_members ORDER BY id"):
		this_id = row[0]
		if this_id not in in_use_ids_members:
			in_use_ids_members.append(this_id)

	print("In use IDs (threads): " + str(in_use_ids))
	print("In use IDs (members): " + str(in_use_ids_members))

	# Loop through threads table on both databases
	# Add entries to destination database with new id
	for row_s in cur_s.execute("SELECT * FROM threads ORDER BY id"):
		match = 0
		for row_d in cur_d.execute("SELECT * FROM threads ORDER BY id"):
			if(row_s[0] == row_d[0]):
				if(row_s[1] == row_d[1]):
					print("ID " + str(row_s[0]) + " matches")
					print("Thread name " + row_s[1] + " matches")
					match = 1
					break
				else:
					print("Thread name conflict at ID " + str(row_s[0]))
					print("Source: " + row_s[1] + ", Destination: " + row_d[1])
					
					# Search destination table to see if this thread name exists
					# at a different ID.  If it does, don't add a new entry.
					for row_d in cur_d.execute("SELECT * FROM threads ORDER BY id"):
						if(row_s[1] == row_d[1]):
							print("Thread name " + row_s[1] + " matched at destination ID " + str(row_d[0]))
							match = 1
							break
							
					if(match == 0):
						new_id = row_s[0] + 1
						while new_id in in_use_ids:
							new_id = new_id + 1

						row_s_list = []
						row_s_list.extend(row_s)
						if(row_s_list[2] == None):
							row_s_list[2] = ''
						row_s_list[0] = new_id
						row_s_list[3] = ''
						row_s_list[7] = ''
						row_s = tuple(row_s_list)
						print(row_s)
						cur_d.execute("INSERT INTO threads VALUES " + str(row_s))
						
						# Find user ID matching the newly-inserted thread and create a thread_members entry
						user_id = 0
						for user_d in cur_d.execute("SELECT id FROM users WHERE username = \'" + row_s[1] + "\'"):
							user_id = user_d[0]
							break
						
						row_members = []
						
						row_members_id = 1
						while row_members_id in in_use_ids_members:
							row_members_id = row_members_id + 1
						
						print("New thread_members entry ID is " + str(row_members_id))

						row_members.append(row_members_id)
						row_members.append(row_s[0])
						row_members.append(user_id)

						cur_d.execute("INSERT INTO thread_members VALUES " + str(tuple(row_members)))
						
						in_use_ids_members.append(row_members_id)
						in_use_ids.append(new_id)
						match = 1
				break

		if(match == 0):
			print("ID " + str(row_s[0]) + " does not match")

			# Search destination table to see if this thread name exists
			# at a different ID.  If it does, don't add a new entry.
			for row_d in cur_d.execute("SELECT * FROM threads ORDER BY id"):
				if(row_s[1] == row_d[1]):
					print("Thread name " + row_s[1] + " matched at destination ID " + str(row_d[0]))
					match = 1
					break

			if(match == 0):
				if row_s[0] not in in_use_ids:
					row_s_list = []
					row_s_list.extend(row_s)
					if(row_s_list[2] == None):
						row_s_list[2] = ''
					row_s_list[3] = ''
					row_s_list[7] = ''
					row_s = tuple(row_s_list)
					print(row_s)
					cur_d.execute("INSERT INTO threads VALUES " + str(row_s))
					
					# Find user ID matching the newly-inserted thread and create a thread_members entry
					user_id = 0
					for user_d in cur_d.execute("SELECT id FROM users WHERE username = \'" + row_s[1] + "\'"):
						user_id = user_d[0]
						break
					
					row_members = []
					
					row_members_id = 1
					while row_members_id in in_use_ids_members:
						row_members_id = row_members_id + 1
					
					print("New thread_members entry ID is " + str(row_members_id))
					
					row_members.append(row_members_id)
					row_members.append(row_s[0])
					row_members.append(user_id)

					cur_d.execute("INSERT INTO thread_members VALUES " + str(tuple(row_members)))
					
					in_use_ids_members.append(row_members_id)
					in_use_ids.append(row_s[0])

def merge_messages(cur_d, cur_s):
	# Create a list of in-use IDs in the destination database
	in_use_ids = []

	for row in cur_d.execute("SELECT id FROM messages ORDER BY id"):
		this_id = row[0]
		if this_id not in in_use_ids:
			in_use_ids.append(this_id)

	print("In-use IDs: " + str(in_use_ids))

	# Loop through messages table on both databases
	# Add entries to destination database with new id
	for row_s in cur_s.execute("SELECT * FROM messages ORDER BY id"):
		print("loop")
		match = 0
			
		# Search destination table to see if this message uid exists
		# at a different ID.  If it does, don't add a new entry.
		for row_d in cur_d.execute("SELECT * FROM messages ORDER BY id"):
			if(row_s[1] == row_d[1]):
				print("Message uid " + row_s[1] + " matched at destination ID " + str(row_d[0]))
				match = 1
				break
				
		if(match == 0):
			new_id = row_s[0] + 1
			while new_id in in_use_ids:
				new_id = new_id + 1

			row_s_list = []
			row_s_list.extend(row_s)
			row_s_list[0] = new_id
			
			# Find thread ID in the source database
			user_id = 0
			for thread_s in cur_s.execute("SELECT * FROM thread_members WHERE thread_id = " + str(row_s_list[2])):
				user_id = thread_s[2]
				break
			
			# Find username from user ID in source database
			username = ""
			for user_s in cur_s.execute("SELECT * FROM users WHERE id = " + str(user_id)):
				username = user_s[1]
				break
				
			print("Username for this message: " + username)
			
			# Find the ID of this username in the destination database
			user_id = 0
			for user_d in cur_d.execute("SELECT * FROM users WHERE username = \'" + username + "\'"):
				user_id = user_d[0]
				break
			
			print("User ID in destination database " + str(user_id))
			
			# Find the thread ID for this user ID in the destination database
			thread_id = 0
			for thread_d in cur_d.execute("SELECT * FROM thread_members WHERE user_id = " + str(user_id)):
				thread_id = thread_d[1]
				break
			
			print("Thread ID for this message in destination database " + str(thread_id))
			
			# Set thread_id of new message row
			row_s_list[2] = thread_id
			
			# Find sender username from sender ID in the source database
			sender_name = ""
			for sender_s in cur_s.execute("SELECT * FROM users WHERE id = " + str(row_s_list[3])):
				sender_name = sender_s[1]
				break
			
			print("Sender name in source database " + sender_name)
			
			# Find sender user ID in the destination database
			sender_id = 0
			for sender_d in cur_d.execute("SELECT * FROM users WHERE username = \'" + sender_name + "\'"):
				sender_id = sender_d[0]
				break
			
			print("Sender ID for this message in destination database " + str(sender_id))
			
			# Set sender_id of new message row
			row_s_list[3] = sender_id
			
			if(row_s_list[4] == None):
				row_s_list[4] = ''
			
			if(row_s_list[9] == None):
				row_s_list[9] = ''
				
			if(row_s_list[11] == None):
				row_s_list[11] = ''

			row_s = tuple(row_s_list)
			print(row_s)
			cur_d.execute("INSERT INTO messages VALUES " + str(row_s))
			
			in_use_ids.append(new_id)						
							
def main():
	db_d = "destination.db"
	db_s = "source.db"

	conn_d = create_connection(db_d)
	conn_s = create_connection(db_s)

	cur_d = conn_d.cursor()
	cur_s = conn_s.cursor()

	print("Merging users table")
	merge_users(cur_d, cur_s)
	
	print("")
	print("Merging threads table")
	merge_threads(cur_d, cur_s)

	print("")
	print("Merging messages table")
	merge_messages(cur_d, cur_s)
	
	conn_d.commit()

	conn_d.close()
	conn_s.close()

if __name__ == "__main__":
	main()
